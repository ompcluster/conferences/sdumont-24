#include <iostream>

/* Time measurement */
#include <chrono>
using namespace std::chrono;
#define START(x)    x = steady_clock::now();
#define END(x)      x = steady_clock::now();
#define TIME(x, y)  duration_cast<microseconds>(x - y).count()

/* Program Start */
void init_array(unsigned int *V, size_t size, unsigned int value) {
  for (int i = 0; i < size; i++)
    V[i] = value;
}

int main(int argc, char *argv[]) {
  steady_clock::time_point begin, end;
  
  size_t size = static_cast<size_t>(std::stoi(argv[1]));

  unsigned int *A = new unsigned int[size];
  unsigned int *B = new unsigned int[size];
  unsigned int *C = new unsigned int[size];

  init_array(A, size, 1);
  init_array(B, size, 2);
  init_array(C, size, 0);

  // Element-wise Sum
  START(begin);
  for (int i = 0; i < size; i++) { 
    C[i] = A[i] + B[i];
  }
  END(end);

  std::cout << "Duration: " << TIME(end, begin) << " µs" <<std::endl;

  delete[] A;
  delete[] B;
  delete[] C;

  return 0;
}