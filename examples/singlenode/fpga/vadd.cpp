#include <iostream>
#include <cassert>

#include "xrt/xrt_bo.h"
#include "xrt/xrt_device.h"
#include "xrt/xrt_kernel.h"
#include <experimental/xrt_xclbin.h>

/* Time measurement */
#include <chrono>
using namespace std::chrono;
#define START(x)    x = steady_clock::now();
#define END(x)      x = steady_clock::now();
#define TIME(x, y)  duration_cast<microseconds>(x - y).count()

/* Program Start */
void init_array(unsigned int *V, size_t size, unsigned int value) {
  for (int i = 0; i < size; i++)
    V[i] = value;
}

int validate(unsigned int *A, unsigned int *B, unsigned int *C, size_t size) {
  for (int i = 0; i < size; i++)
    if (C[i] != (A[i] + B[i]))
      return 1;
  return 0;
}

int main(int argc, char *argv[]) {
  steady_clock::time_point begin, end;
  steady_clock::time_point kbegin, kend;

  size_t size = static_cast<size_t>(std::stoi(argv[1]));
  
  // Host buffers that will be mapped to FPGA need to be align allocated
  unsigned int *A;
  assert(!posix_memalign((void **)&A, 4096, size * sizeof(unsigned int)));
  init_array(A, size, 1);
  unsigned int *B;
  assert(!posix_memalign((void **)&B, 4096, size * sizeof(unsigned int)));
  init_array(B, size, 2);
  unsigned int *C;
  assert(!posix_memalign((void **)&C, 4096, size * sizeof(unsigned int)));
  init_array(C, size, 0);

  // FPGA Device and Kernel Setup
  xrt::device dev(0);
  auto file_info = xrt::xclbin("/mirror/sdumont/examples/singlenode/fpga/vadd.xclbin");
  auto uuid = dev.load_xclbin(file_info);
  // Get FPGA kernel and run object
  xrt::kernel k(dev, uuid.get(), "vadd_hw");
  xrt::run r(k);
 
  START(begin);
  // Setup FPGA buffers
  xrt::bo A_bo = xrt::bo(dev, A, size * sizeof(unsigned int), xrt::bo::flags::normal, k.group_id(0));
  xrt::bo B_bo = xrt::bo(dev, B, size * sizeof(unsigned int), xrt::bo::flags::normal, k.group_id(1));
  xrt::bo C_bo = xrt::bo(dev, C, size * sizeof(unsigned int), xrt::bo::flags::normal, k.group_id(2));

  // Sync HOST -> FPGAs
  A_bo.sync(XCL_BO_SYNC_BO_TO_DEVICE);
  B_bo.sync(XCL_BO_SYNC_BO_TO_DEVICE);

  // Set arguments
  r.set_arg(0, A_bo);
  r.set_arg(1, B_bo);
  r.set_arg(2, C_bo);
  r.set_arg(3, size);

  // Start kernel run and wait for it to complete
  START(kbegin);
  r.start();
  r.wait();
  END(kend);

  C_bo.sync(XCL_BO_SYNC_BO_FROM_DEVICE);
  END(end);

  std::cout << "Duration Total: " << TIME(end, begin) << " µs" <<std::endl;
  std::cout << "Duration Kernel Only: " << TIME(kend, kbegin) << " µs" <<std::endl;
  
  int result = validate(A, B, C, size);

  free(A);
  free(B);
  free(C);

  return result;
}