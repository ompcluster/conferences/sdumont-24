#include <iostream>

/* Time measurement */
#include <chrono>
using namespace std::chrono;
#define START(x)    x = steady_clock::now();
#define END(x)      x = steady_clock::now();
#define TIME(x, y)  duration_cast<microseconds>(x - y).count()

/* Program Start */
void vadd_hw(unsigned int *A, unsigned int *B, unsigned int *C, size_t size);
#pragma omp declare variant(vadd_hw) match(device = {arch(alveo)})
void vadd(unsigned int *A, unsigned int *B, unsigned int *C, size_t size) {
  for (int i = 0; i < size; i++)
    C[i] = A[i] + B[i];
}

void init_array(unsigned int *V, size_t size, unsigned int value) {
  for (int i = 0; i < size; i++)
    V[i] = value;
}

int validate(unsigned int *A, unsigned int *B, unsigned int *C, size_t size) {
  for (int i = 0; i < size; i++) {
    if (C[i] != (A[i] + B[i])) {
      std::cout << "Error in position " << i << " - Value: expected(" << (A[i] + B[i]) << ")" << " was(" << C[i] << ")\n";
      return 1;
    }
  }
  return 0;
}
int main(int argc, char *argv[]) {
  steady_clock::time_point begin, end;
  
  size_t size = static_cast<size_t>(std::stoi(argv[1]));
  size_t num_fpgas = static_cast<size_t>(std::stoi(argv[2]));

  unsigned int *A = new unsigned int[size];
  unsigned int *B = new unsigned int[size];
  unsigned int *C = new unsigned int[size];

  init_array(A, size, 1);
  init_array(B, size, 2);
  init_array(C, size, 0);

  int block_size = size / num_fpgas;

  // Element-wise Sum
  START(begin);
#pragma omp parallel
#pragma omp single
  {
    for (int i = 0; i < num_fpgas; i++) {
      unsigned int *lA = &A[i * block_size];
      unsigned int *lB = &B[i * block_size];
      unsigned int *lC = &C[i * block_size];

#pragma omp target map(to: lA[:block_size], lB[:block_size])             \
                   map(from: lC[:block_size])                           \
                   depend(in: *lA, *lB) depend(inout: *lC) nowait
      vadd(lA, lB, lC, block_size);
    }
  }
  END(end);

  std::cout << "Duration: " << TIME(end, begin) << " µs" <<std::endl;
  
  int result = validate(A, B, C, size);

  delete[] A;
  delete[] B;
  delete[] C;

  return result;
}