#include <iostream>
#include <mpi.h>

/* Time measurement */
#include <chrono>
using namespace std::chrono;
#define START(x)    x = steady_clock::now();
#define END(x)      x = steady_clock::now();
#define TIME(x, y)  duration_cast<microseconds>(x - y).count()

/* Program Start */
void init_array(unsigned int *V, size_t size, unsigned int value) {
  for (int i = 0; i < size; i++)
    V[i] = value;
}

int validate(unsigned int *A, unsigned int *B, unsigned int *C, size_t size) {
  for (int i = 0; i < size; i++)
    if (C[i] != (A[i] + B[i]))
      return 1;
  return 0;
}

int main(int argc, char *argv[]) {
  steady_clock::time_point begin, end;
  steady_clock::time_point kbegin, kend;

  /* Start of MPI */
  MPI_Init(NULL, NULL);

  int mpi_rank, mpi_size;
  MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
  MPI_Barrier(MPI_COMM_WORLD);

  // Size and buffers
  size_t size = static_cast<size_t>(std::stoi(argv[1]));
  int size_node = size / mpi_size;
  int bsize = mpi_rank == 0 ? size : size_node;

  /* Buffer Definitions 
      This will depend on the application. In distributed applications, it is common that the root read the data and then distribute the data across the available nodes for computation. In our case, we are creating sythetic data only in Rank 0 (first node). 
      Many applications operate on large amount of data, so this becomes a relevant piece of evaluation during the distributed decisions (where to read, where to move, etc).
  */
  unsigned int *A = new unsigned int[bsize];
  unsigned int *B = new unsigned int[bsize];
  unsigned int *C = new unsigned int[bsize];

  if (mpi_rank == 0) {
    init_array(A, size, 1);
    init_array(B, size, 2);
    init_array(C, size, 0);
  }

  START(begin);
  // Scatter data - Collective
  MPI_Scatter(A, size_node, MPI_UNSIGNED, mpi_rank == 0 ? MPI_IN_PLACE : A, size_node, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
  MPI_Scatter(B, size_node, MPI_UNSIGNED, mpi_rank == 0 ? MPI_IN_PLACE : B, size_node, MPI_UNSIGNED, 0, MPI_COMM_WORLD);

  // Element-wise Sum
  START(kbegin);
  for (int i = 0; i < size_node; i++) { 
    C[i] = A[i] + B[i];
  }
  END(kend);

  MPI_Gather(mpi_rank == 0 ? MPI_IN_PLACE : C, size_node, MPI_UNSIGNED, C, size_node, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
  END(end);

  int result = 0;

  if (mpi_rank == 0) {
    std::cout << "Duration Total: " << TIME(end, begin) << " µs" <<std::endl;
    std::cout << "Duration Kernel Only: " << TIME(kend, kbegin) << " µs" <<std::endl;
    result = validate(A, B, C, size);
  }

  delete[] A;
  delete[] B;
  delete[] C;

  MPI_Finalize();
  return result;
}