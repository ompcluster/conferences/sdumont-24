#include <iostream>
#include <cassert>
#include <mpi.h>

#include "xrt/xrt_bo.h"
#include "xrt/xrt_device.h"
#include "xrt/xrt_kernel.h"
#include <experimental/xrt_xclbin.h>

/* Time measurement */
#include <chrono>
using namespace std::chrono;
#define START(x)    x = steady_clock::now();
#define END(x)      x = steady_clock::now();
#define TIME(x, y)  duration_cast<microseconds>(x - y).count()

/* Program Start */
void init_array(unsigned int *V, size_t size, unsigned int value) {
  for (int i = 0; i < size; i++)
    V[i] = value;
}

int validate(unsigned int *A, unsigned int *B, unsigned int *C, size_t size) {
  for (int i = 0; i < size; i++) {
    if (C[i] != (A[i] + B[i])) {
      std::cout << "Error in position " << i << " - Value: expected(" << (A[i] + B[i]) << ")" << " was(" << C[i] << ")\n";
      return 1;
    }
  }
  return 0;
}

int main(int argc, char *argv[]) {
  steady_clock::time_point begin, end;
  steady_clock::time_point sbegin, send;
  steady_clock::time_point kbegin, kend;

  /* ----------------- MPI */
  MPI_Init(NULL, NULL);

  int mpi_rank, mpi_size;
  MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
  MPI_Barrier(MPI_COMM_WORLD);

  /* ----------------- FPGA Init */
  int device_id = mpi_rank / 3; // We have 3 nodes for 6 processes and 6 FPGAs
  xrt::device dev(device_id);
  auto file_info = xrt::xclbin("/mirror/sdumont/examples/singlenode/fpga/vadd.xclbin");
  auto uuid = dev.load_xclbin(file_info);
  // Get FPGA kernel and run object
  xrt::kernel k(dev, uuid.get(), "vadd_hw");
  xrt::run r(k);
 
  /* ----------------- Buffers */
  size_t size = static_cast<size_t>(std::stoi(argv[1]));
  int size_node = size / mpi_size;
  int bsize = mpi_rank == 0 ? size : size_node;
  unsigned int *A = NULL;
  unsigned int *B = NULL;
  unsigned int *C = NULL;
  assert(!posix_memalign((void **)&A, 4096, bsize * sizeof(unsigned int)));
  assert(!posix_memalign((void **)&B, 4096, bsize * sizeof(unsigned int)));
  assert(!posix_memalign((void **)&C, 4096, bsize * sizeof(unsigned int)));

  if (mpi_rank == 0) {
    init_array(A, size, 1);
    init_array(B, size, 2);
    init_array(C, size, 0);
  }

  /* ----------------- Divide and conquer */
  START(begin);
  // Scatter data - Collective
  MPI_Scatter(A, size_node, MPI_UNSIGNED, mpi_rank == 0 ? MPI_IN_PLACE : A, size_node, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
  MPI_Scatter(B, size_node, MPI_UNSIGNED, mpi_rank == 0 ? MPI_IN_PLACE : B, size_node, MPI_UNSIGNED, 0, MPI_COMM_WORLD);


  /* ----------------- FPGA Buffer creating & Staging */
  START(sbegin);
  xrt::bo A_bo = xrt::bo(dev, A, size_node * sizeof(unsigned int), xrt::bo::flags::normal, k.group_id(0));
  xrt::bo B_bo = xrt::bo(dev, B, size_node * sizeof(unsigned int), xrt::bo::flags::normal, k.group_id(1));
  xrt::bo C_bo = xrt::bo(dev, C, size_node * sizeof(unsigned int), xrt::bo::flags::normal, k.group_id(2));

  A_bo.sync(XCL_BO_SYNC_BO_TO_DEVICE);
  B_bo.sync(XCL_BO_SYNC_BO_TO_DEVICE);

  /* ----------------- Kernel Config & Execution */
  r.set_arg(0, A_bo);
  r.set_arg(1, B_bo);
  r.set_arg(2, C_bo);
  r.set_arg(3, size_node);

  // Start kernel run and wait for it to complete
  START(kbegin);
  r.start();
  r.wait();
  END(kend);

  C_bo.sync(XCL_BO_SYNC_BO_FROM_DEVICE);
  END(send);

  MPI_Gather(mpi_rank == 0 ? MPI_IN_PLACE : C, size_node, MPI_UNSIGNED, C, size_node, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
  END(end);

  int result = 0;

  if (mpi_rank == 0) {
    std::cout << "Duration Total: " << TIME(end, begin) << " µs" <<std::endl;
    std::cout << "Duration with Staging: " << TIME(send, sbegin) << " µs" <<std::endl;
    std::cout << "Duration Pure Kernel: " << TIME(kend, kbegin) << " µs" <<std::endl;
    result = validate(A, B, C, size);
  }

  free(A);
  free(B);
  free(C);

  MPI_Finalize();
  return result;
}