## **FPGA-VADD**
HLS implementation of a basic element-wise vector addition kernel. Following commands would build the bitstream for the u55c platform compatible with Vitis 2022.1:
```shell
# Assuming Xilinx tools are sourced
make -f Makefile.bitstream
```
**PS:** This example should take 1h to build

### **For more examples regarding HLS implementation of FPGA kernels**
- [Vitis Tutorials Page](https://xilinx.github.io/Vitis-Tutorials/2022-1/build/html/index.html)
- [Vitis Tutorials Repo](https://github.com/Xilinx/Vitis-Tutorials)