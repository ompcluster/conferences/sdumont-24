
# /*******************************************************************************
#  Copyright (C) 2021 Xilinx, Inc
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#
# *******************************************************************************/

PLATFORM?=xilinx_u55c_gen3x16_xdma_3_202210_1
FREQUENCY=250
BUILD_DIR=link_vadd
CONFIGFILE=./vadd.cfg
VADD_XO=./vadd.xo
XCLBIN=./vadd.xclbin
TARGET=ip

ifeq (u250,$(findstring u250, $(PLATFORM)))
	FPGAPART=xcu250-figd2104-2L-e
else ifeq (u280,$(findstring u280, $(PLATFORM)))
	FPGAPART=xcu280-fsvh2892-2L-e
else ifeq (u55c,$(findstring u55c, $(PLATFORM)))
	FPGAPART=xcu55c-fsvh2892-2L-e
else
	$(error Unsupported PLATFORM)
endif

all: $(VADD_XO) $(XCLBIN)

$(VADD_XO): build_vadd.tcl vadd.cpp
	vitis_hls $< -tclargs $(TARGET) $(FPGAPART)

$(XCLBIN): $(VADD_XO)
	v++ -l -t hw --platform $(PLATFORM) --kernel_frequency $(FREQUENCY) --save-temps --temp_dir $(BUILD_DIR) --config $(CONFIGFILE) $^ -o $@

clean:
	rm -rf *.xclbin*
	rm -rf _x
	rm -rf .ipcache
	rm -rf *.xo
	rm -rf *.log
	rm -rf *.ltx
	rm -rf .Xil
	rm -rf link_vadd
	rm -rf build_vadd